const { useEffect, useState } = require("react")
const io = require("socket.io-client")
const socket = io()

const useSocket = (callback) => {
	const [activeSocket, setActiveSocket] = useState(null)

	useEffect(() => {
		if (activeSocket || !socket) return
		callback && callback(socket)
		setActiveSocket(socket)
		return function cleanup() {
			socket.off("message.chat1", callback)
		}
	}, [socket])

	return activeSocket
}

module.exports = useSocket