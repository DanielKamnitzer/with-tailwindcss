import React, {useState, useEffect} from "react"
import {useRouter} from 'next/router'
import {useSocket} from "../../hooks"
// import Chart from "react-apexcharts"

export default function Device() {
    const [data, setData] = useState()
    const [options] = useState({
        xaxis: {
            type: "datetime"
        },
        stroke: {
            curve: "smooth",
        },
        fill: {
            type: "gradient"
        },
        chart: {
            toolbar: {
                show: false
            }
        }
    })
    const router = useRouter()
	const io = useSocket()

    useEffect(() => {
        if(io) {
            io.emit("initial", [{
                id: router.query.id,
                seconds: 24 * 60 * 60
            }])
            io.on("initial", initData)
            io.on("update", updateData)
            // setTimeout(() => io.emit("update", {device: 21, data: {"ExampleTag1": "50.00", "time": "2021-04-22 14:31:18"}}), 5000)
        }
    }, [io])

    const sortData = (data) => {
        return data.sort((a, b) => {
            if(a.timestamp < b.timestamp){
                return -1
            } else if(a.timestamp > b.timestamp) {
                return 1
            }
            return 0
        })
    }

    const initData = (init) => {
        setData(previous => ({
            ...previous,
            [init.device.id]: sortData(init.data)
        }))
    }

    const updateData = (update) => {
        setData(previous => ({
            ...previous,
            [update.device.id]: [
                ...previous[update.device.id],
                update
            ]
        }))
    }

    const series = () => {
        return Object.keys(data).map(device => ({
            name: `device ${device}`,
            data: data[device].map(dataset => ({
                x: new Date(dataset.data.time.replace(" ", "T") + "Z"),
                y: dataset.data.AussenTemperatur
            }))
        }))
    }

    return (
        <div className="dashboard">
            {data ? 
                <div className="chart">
                    {/* <Chart
                        options={options}
                        series={series()}
                        type="area"
                    /> */}
                    <pre>{JSON.stringify(data, null, 2)}</pre>
                </div>
            : "loading data"}
        </div>
    )
}