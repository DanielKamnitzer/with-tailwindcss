class Sockets {
    constructor() {
        this.sockets = {}
        this.devices = {}
    }

    addSocket(devices, socket) {
        for(const device of devices) {
            console.log(socket.id)
            !this.deviceExists(device.id) && this.createDevice(device.id)
            this.sockets[device.id][socket.id] = socket
            console.log(`[CONNECTION] ${this.countSockets(device.id)} clients connected for device ${device.id} (${(new Date()).toLocaleString()})`)
        }
        this.devices[socket.id] = devices
    }

    removeSocket(socket) {
        const devices = this.devices[socket.id]
        if(devices && devices.length > 0) {
            for(const device of devices) {
                delete this.sockets[device.id][socket.id]
                console.log(`[DISONNECTION] ${this.countSockets(device.id)} clients connected for device ${device.id} (${(new Date()).toLocaleString()})`)
            }
            delete this.devices[socket.id]
            return devices
        }
    }

    countSockets(deviceId) {
        if(this.deviceExists(deviceId)) {
            return Object.keys(this.sockets[deviceId]).length
        }
        return 0
    }

    createDevice(deviceId) {
        this.sockets[deviceId] = {}
    }

    deviceExists(deviceId) {
        return typeof this.sockets[deviceId] === "object"
    }

    emitData(deviceId, event, data) {
        if(this.countSockets(deviceId) > 0) {
            for(const id in this.sockets[deviceId]) {
                this.sockets[deviceId][id].emit(event, data)
                console.log(`[UPDATE] sent to ${this.countSockets(deviceId)} clients for device ${deviceId} (${(new Date()).toLocaleTimeString()})`)
            }
        }
    }
}

module.exports = Sockets