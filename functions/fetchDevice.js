const AWS = require("aws-sdk")
const config = require("../config")
const client = new AWS.DynamoDB.DocumentClient(config.AWS)

const fetchDevice = (device) => {
    return new Promise((resolve, reject) => {
        const params = {
            TableName: "data",
            ExpressionAttributeValues: {
                ":device": device.id,
                ":timestamp": Date.now() - device.seconds * 1000
            }, 
            ExpressionAttributeNames: {
                "#timestamp": "timestamp"
            },
            FilterExpression: "device = :device AND #timestamp > :timestamp", 
        }
    
        client.scan(params, function (error, data) {
            if(error) {
                reject(error)
            } else {
                resolve(data)
            }
        })
    })
}

module.exports = fetchDevice