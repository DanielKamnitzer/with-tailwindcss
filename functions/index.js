const fetchDevice = require("./fetchDevice")
const listDevices = require("./listDevices")
const updateDevice = require("./updateDevice")

module.exports = {fetchDevice, listDevices, updateDevice}