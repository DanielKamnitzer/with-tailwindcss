const AWS = require("aws-sdk")
const config = require("../config")
const client = new AWS.DynamoDB.DocumentClient(config.AWS)

const updateDevice = (device, data) => {
    return new Promise((resolve, reject) => {
        const update = {
            timestamp: Date.now(),
            device: device,
            data: data
        }
        client.put({TableName: "data", Item: update}, (error, response) => {
            if(error) {
                reject(error)
            } else {
                resolve(response)
            }
        })
    })
}

module.exports = updateDevice