const AWS = require("aws-sdk");
const config = require("../config")
const client = new AWS.Iot(config.AWS)

const listDevices = () => {
    return new Promise((resolve, reject) => {
        client.listThings((error, data) => {
            if(error) {
                reject(error)
            } else {
                resolve(data.things)
            }
        })
    })
}

module.exports = listDevices
