const app = require("express")()
const server = require("http").Server(app)
const io = require("socket.io")(server)
const next = require("next")
const {fetchDevice, listDevices, updateDevice} = require("./functions") 
const {Sockets} = require("./classes")

const port = parseInt(process.env.PORT, 10) || 3000
const dev = process.env.NODE_ENV !== "production"
const nextApp = next({ dev })
const nextHandler = nextApp.getRequestHandler()
const sockets = new Sockets()

io.on("connection", async (socket) => {
	//TODO: devices by user, maybe device by single get without sockets
	socket.on("devices", async (user) => {
		socket.emit("devices", await listDevices())
	})
	socket.on("initial", async (devices) => {
		sockets.addSocket(devices, socket)
		for(device of devices) {
			const response = await fetchDevice(device)
			socket.emit("initial", {device: device, data: response.Items})
		}
	})
	socket.on("update", async (update) => {
		await updateDevice(update.device, update.data)
	})
	socket.on("disconnect", () => sockets.removeSocket(socket))
})

nextApp.prepare().then(() => {
	app.post("/api/update", (request, response) => {
		const update = {
			data: request.body,
			timestamp: request.headers.timestamp,
			device: request.headers.device
		}
		sockets.emitData(update.device, "update", update)
		response.send(`[UPDATE] sent to ${sockets.countSockets(update.device)} clients for device ${update.device} (${(new Date()).toLocaleTimeString()})`)
	})

	app.get("*", (req, res) => {
		return nextHandler(req, res)
	})

	server.listen(port, err => {
		if (err) throw err
		console.log(`> Ready on http://localhost:${port}`)
	})
})